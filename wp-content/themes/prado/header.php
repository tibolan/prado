<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?><!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php wp_title('|', true, 'right'); ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11"/>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
    <link href='http://fonts.googleapis.com/css?family=GFS+Didot' rel='stylesheet' type='text/css'/>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/webq/css/default.css"/>
    <script src="<?php bloginfo('template_directory'); ?>/webq/js/vendor/modernizr-2.6.2.min.js"></script>
    <script>
        var isMobile = <?php echo (is_mobile()) ? "true" : "false"; ?>;
        var isReallyMobile = <?php echo (is_really_mobile()) ? "true" : "false"; ?>;
        document.documentElement.className += (isReallyMobile) ? " isReallyMobile" : (isMobile) ? " isTablet" : "";
    </script>


</head>

<body class="<?php $UNIVERS; ?>">

<header>
    <nav role="navigation" id="nav">
        <a href="#main" class="visuallyhidden focusable">Accès direct au contenu</a>
        <a href="#navFirst" id="showNav" class="visuallyhidden focusable">Accès direct à la navigation</a>
        <ul>
            <li id="home">
                <a href="/" id="navFirst">
                    <img src="<?php echo get_option("options_logo"); ?>"/>
                </a>
            </li>
            <li class="left">
                <ul>
                    <?php

                    $args = array(
                        "post_type" => "page",
                        "orderby" => "menu_order",
                        "order" => "ASC"
                    );

                    $espaceProUrl = "#login";

                    $the_query = new WP_Query($args);
                    $i = -1;

                    while ($the_query->have_posts()) : $the_query->the_post();

                        $id = get_the_ID();
                        $cat = get_post_meta($id, "univers", true);
                        //echo "<li>cat=$cat</li>";

                        if ($cat == 24) continue; // skip home
                        $dispatchLink = get_permalink($id);


                        $dispatchLinkTitle = get_the_title($id);
                        $args2 = array(
                            "post_type" => "post",
                            "cat" => $cat
                        );
                        $the_category_query = new WP_Query($args2);
                        $number_of_posts = $the_category_query->post_count;
                        $i++;

                        $pt = (is_user_logged_in() ? 4 : 3);

                        if ($i == $pt) echo "</ul></li><li class='right'><ul>";

                        // TODO current si l'url du post == current url

                        // if no post don't open <li>
                        echo "<li class='" . ($cat == 25 ? "visuallyhidden " : "") . "item-" . ($i) . "'>";

                        // if no post, the link point to post permalink
                        if ($number_of_posts == 0) {
                            $the_category_query->the_post();
                            if ($cat == 25) $espaceProUrl = $dispatchLink;
                            echo "<a href='" . $dispatchLink . "'>" . $dispatchLinkTitle . "</a>";

                        } // if many posts, build a link to dispatch and a list of links to posts
                        else if ($number_of_posts == 1) {
                            $the_category_query->the_post();
                            $url = get_permalink(get_the_ID());
                            if ($cat == 25) $espaceProUrl = $url;
                            echo "<a href='" . $url . "'>" . $dispatchLinkTitle . "</a>";
                        } // if many posts, build a link to dispatch and a list of links to posts
                        else if ($number_of_posts > 1) {
                            if ($cat == 25) $espaceProUrl = $dispatchLink;
                            echo "<a href='" . $dispatchLink . "'>" . $dispatchLinkTitle . "</a>";
                            echo "<ul>";


                            while ($the_category_query->have_posts()) : $the_category_query->the_post();
                                echo "<li><a href='" . get_permalink(get_the_ID()) . "'>" . get_the_title() . "</a></li>";
                            endwhile;
                            echo "</ul>";
                            //echo "<ul><li><a href='#'>tata</a></li><li><a href='#'>titi</a></li></ul>";
                            // n posts, donc 1 lien vers dispatvh
                            // ul > li > a vers chaque post
                        }

                        // if no post don't close <li>
                        if ($number_of_posts != 0) echo "</li>";



                        /*
                        if($number_of_posts > 1) {
                            echo "<ul>";
                            while ( $the_category_query->have_posts() ) : $the_category_query->the_post();
                                $id2 = get_the_ID();

                                echo "<li>";
                                echo "<a href='".get_permalink($id2)."'>".get_the_title($id2)."</a>";
                                echo "</li>";
                            endwhile; // end of the loop.


                            echo "</ul>";
                        }
                        else {
                            echo "<a href='".$dispatchLink."'>".$dispatchLinkTitle."</a>";
                        } */


                    endwhile; // end of the loop.


                    ?></ul>
            </li>
        </ul>
    </nav>
    <?php
    $logged = is_user_logged_in();?>
    <div id="espacePro" class="<?php echo($logged ? 'logged' : '') ?>">
        <a href="<?php echo $espaceProUrl; ?>" class="<?php echo($logged ? '' : 'opener')?>">Espace Pro</a>
        <a href="<?php echo wp_logout_url($_SERVER["REQUEST_URI"]); ?>">Déconnexion</a>
    </div>


    <div id="login" class="<?php echo($logged ? 'logged' : '') ?>">


        <div class="right">
            <?php $args = array(
                'echo' => true,
                'redirect' => site_url( "espace-pro/docs/" ),
                'form_id' => 'loginform',
                'label_username' => __( 'Login' ),
                'label_password' => __( 'Mot de passe' ),
                'label_remember' => __( 'Se souvenir de moi' ),
                'label_log_in' => __( 'Connexion' ),
                'id_username' => 'user_login',
                'id_password' => 'user_pass',
                'id_remember' => 'rememberme',
                'id_submit' => 'wp-submit',
                'remember' => true,
                'value_username' => NULL,
                'value_remember' => false );



            wp_prado_login( $args );



            ?>


            <!--form method="post" action="">

                <legend>Login Membre</legend>

                <fieldset>
                    <p><input type="text" required name="log" placeholder="login" value=""/></p>

                    <p><input type="text" required name="pwd" placeholder="mot de passe" value=""/></p>

                    <p class="check"><label><input type="checkbox" name="rememberme"/>Se souvenir de moi</label></p>

                    <p><input type="submit" value="Connexion" class="button"/><a href="forgotPwd.php">Mot de passe
                            oublié ?</a></p>
                </fieldset>
            </form-->
            <a id="loginClose">Fermer</a>
        </div>
        <div class="left">
            <?php echo do_shortcode('[contact-form-7 class="signin" id="207" title="Contact form 1"]'); ?>
        </div>


    </div>

</header>
<main>
