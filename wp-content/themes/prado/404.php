<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>
    <div class="tpl-info noway" id="p404">
        <div style="max-width:60%;margin: 0 auto;">
            <h1>Erreur serveur</h1>

            <p>La page que vous recherchez n'existe pas ou nécessite d'être enregistré.</p>

            <!--div class="login">
                <?php echo do_shortcode('[contact-form-7 class="signin" id="207" title="Contact form 1"]'); ?>
            </div-->
        </div>
    </div>


<?php get_footer(); ?>

<script>
    var infoMainSize = function (){
        var w = $(window).height();
        var h = $("header").height();
        var f = $("footer").height();
        var i = $(".identity").height();
        var H = w - h - f - 133; // 133 are paddings & margins somewhere...
        $("#p404").css({
            position: "relative",
            overflow: "auto",
            "min-height": H,
            "max-height": H,
            background: "#0f9095"
        });

        $(".identity").css({
            marginTop: (H - i) / 2
        });
    }
    $(window).resize(infoMainSize);
    infoMainSize();
</script>