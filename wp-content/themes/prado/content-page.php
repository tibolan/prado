<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>


<section>

    <?php

    $showCarrousel = get_field("carrousel_activer");
    $carrousel = get_field("carrousel");
    $univers = get_field("univers");


    if ($showCarrousel && !is_really_mobile()  ) createCarrousel($carrousel, "inBanner");


    $args = array(
        'category_name' => $univers->slug
    );

    // The Query
    $the_query = new WP_Query($args);
    $l = count($the_query->posts);

    if ($l > 4) $l = 4;
    else if ($l == 3) $l = 2;

    echo "<div class='postList postList_By$l'>";


    echo "<ul>";


    // The Loop
    if ($the_query->have_posts()) {
        while ($the_query->have_posts()) {
            $the_query->the_post();
            $title = get_the_title();
            $desc = get_field("description_courte");
            $img = get_field("banniere_haute");

            ?>
            <li>
                <a href="<?php the_permalink();?>">
                    <div class="header">
                        <h2><?php echo $title;?></h2>

                        <p><?php echo $desc; ?></p>
                    </div>
                    <?php if(!is_mobile()){ ?>
                        <div class="image" style="background-image: url(<?php echo $img["url"]; ?>)"></div>
                    <?php };?>

                </a>
            </li>
        <?php
        }
    } else {
        // no posts found
    }
    echo "</ul></div>";

    /* Restore original Post Data */
    wp_reset_postdata();


    ?>


</section>
