<?php
/**
 * Template Name: Infos
 *
 * Description: Template Info pratique
 *
 * @package WordPress
 * @subpackage Prado
 */


get_header();

the_post();
?>


<div class="tpl-info">

    <div class="identity" itemscope itemtype="http://schema.org/Organization">

        <p itemprop="name"><?php echo get_field("nom_de_la_societe"); ?></p>

        <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
            <p itemprop="streetAddress"><?php echo get_field("adresse"); ?><br/><?php echo get_field("adresse_comp"); ?>
            </p>

            <p>
                <span itemprop="postalCode"><?php echo get_field("code_postal"); ?></span>
                <span itemprop="addressLocality"><?php echo get_field("ville"); ?></span>
            </p>

            <p itemprop="addressCountry"><?php echo get_field("pays"); ?></p>
        </div>

        <div>TEL: <a href="tel://<?php echo get_field("numero_de_telephone"); ?>"
                     itemprop="telephone"><?php echo get_field("numero_de_telephone"); ?></a></div>
        <div>FAX: <a href="tel://<?php echo get_field("numero_de_fax"); ?>"
                     itemprop="faxNumber"><?php echo get_field("numero_de_fax"); ?></a></div>

        <div><a href="http://<?php echo get_field("url"); ?>" itemprop="url"><?php echo get_field("url"); ?></a></div>
        <div>
            <a href="<?php echo getGMAP_links(get_field("adresse"), get_field("code_postal"), get_field("ville"));?>">Lien vers Google Map</a></div>
    </div>


    <div class="identity" itemscope itemtype="http://schema.org/Organization">

        <p itemprop="name"><?php echo get_field("nom_de_la_societe_2"); ?></p>

        <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
            <p itemprop="streetAddress"><?php echo get_field("adresse_2"); ?>
                <br/><?php echo get_field("adresse_comp_2"); ?>
            </p>

            <p>
                <span itemprop="postalCode"><?php echo get_field("code_postal_2"); ?></span>
                <span itemprop="addressLocality"><?php echo get_field("ville_2"); ?></span>
            </p>

            <p itemprop="addressCountry"><?php echo get_field("pays_2"); ?></p>
        </div>

        <div>TEL: <a href="tel://<?php echo get_field("numero_de_telephone_2"); ?>"
                     itemprop="telephone"><?php echo get_field("numero_de_telephone_2"); ?></a></div>

        <?php
        $fax2 = get_field("numero_de_fax_2");
        if (strlen($fax2)) {
            ?>
            <div>FAX: <a href="tel://<?php echo get_field("numero_de_fax_2"); ?>"
                         itemprop="faxNumber"><?php echo get_field("numero_de_fax_2"); ?></a></div>
        <?php
        }
        ?>


        <div><a href="http://<?php echo get_field("url_2"); ?>" itemprop="url"><?php echo get_field("url_2"); ?></a>
        </div>
        <div>
            <a href="<?php echo getGMAP_links(get_field("adresse_2"), get_field("code_postal_2"), get_field("ville_2"));?>">Lien vers Google Map</a></div>
    </div>

    <div class="identity_fake"><!-- KEEP ME ALIVE PLZ--></div>
    <?php
    $rte = get_field("texte_libre");
    if (strlen($rte)) {
        ?>
        <div class="rte">
            <?php echo $rte; ?>
        </div>
    <?php
    }
    ?>


</div>







<?php get_footer(); ?>

<script>
    var infoMainSize = function () {
        var w = $(window).height();
        var h = $("header").height();
        var f = $("footer").height();
        var i = $(".identity").height();
        var H = w - h - f - 133; // 133 are paddings & margins somewhere...
        $(".tpl-info").css({
            position: "relative",
            overflow: "auto",
            "min-height": H,
            "max-height": H,
            background: "#0f9095",
            width: "100%"
        });

        $(".identity_fake").css({
            height: H
        });


        if (webq.browser.isMobile) $("main").css({"min-height": 400, "overflow": "hidden"})
    }
    $(window).resize(infoMainSize);
    infoMainSize();
</script>