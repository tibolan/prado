<?php


get_header(); ?>
    <h1>PRO</h1>



<?php while (have_posts()) : the_post(); ?>


   <ul class="docs">
   <?php


        $docs = get_field("telecharger_des_documents");
        foreach($docs as $doc){
            $id = $doc["document"]["id"];
            $url = $doc["document"]["url"];
            $title = $doc["document"]["title"];

            $ext = explode("/", get_post_mime_type($id));
            $ext = $ext[1];
            $size = $doc["document"]["url"];
            $size = intval(filesize( get_attached_file($id) )/1000);
            ?>
                <li>
                    <span class="title"><?php echo $title;?></span>
                    <span class="metas">Format: <?php echo $ext;?></span>
                    <span class="metas">Poids: <?php echo $size;?>ko</span>
                    <a download href="<?php echo $url;?>" data-type="<?php echo $ext;?>" data-size="<?php echo $size;?>">Télécharger</a>
                    <a target="_blank" href="<?php echo $url;?>" data-type="<?php echo $ext;?>" data-size="<?php echo $size;?>" title="Ouvrir (Ctrl + click pour ouvrir dans un nouvel onglet)">Ouvrir</a>
                </li>
            <?php



        }


    ?>
   </ul>



<?php endwhile; // end of the loop. ?>




<?php get_footer(); ?>