<?php
/**
 * Template Name: Home
 *
 * Description: Template de home
 *
 * @package WordPress
 * @subpackage Prado
 */

get_header(); ?>


<?php

the_post();


$showCarrousel = get_field("carrousel_activer");
$carrousel = get_field("carrousel");
$univers = get_field("univers");


if ($showCarrousel && !is_really_mobile()) createCarrousel($carrousel, "inBanner");
?>
    <h1 class="visuallyhidden"><?php echo get_bloginfo('name'); ?></h1>
    <div class="homeBottom">
        <div class="postList postList_By3">
            <ul>

                <li>
                    <?php
                    wp_reset_query();
                    $cat = get_field("choix_de_la_categorie");

                    $the_query = new WP_Query("p=" . $cat->ID);

                            $the_query->the_post();
                            $title = get_the_title();
                            $desc = get_field("description_courte");
                            $img = get_field("banniere_haute");

                            ?>

                            <a href="<?php the_permalink(); ?>">
                                <div class="header">
                                    <h2><?php echo $title; ?></h2>

                                    <p><?php echo $desc; ?></p>
                                </div>
                                <?php if(!is_mobile()){ ?>
                                    <div class="image" style="background-image: url(<?php echo $img["url"]; ?>)"></div>
                                <?php };?>

                            </a>
                </li>
                <li>
                    <?php
                    wp_reset_query();
                    $cat = get_field("choix_de_la_categorie2");

                    $the_query = new WP_Query("p=" . $cat->ID);

                    $the_query->the_post();
                    $title = get_the_title();
                    $desc = get_field("description_courte");
                    $img = get_field("banniere_haute");

                    ?>

                    <a href="<?php the_permalink(); ?>">
                        <div class="header">
                            <h2><?php echo $title; ?></h2>

                            <p><?php echo $desc; ?></p>
                        </div>
                        <?php if(!is_mobile()){ ?>
                            <div class="image" style="background-image: url(<?php echo $img["url"]; ?>)"></div>
                        <?php };?>
                    </a>
                </li>
                <li>
                    <?php
                    wp_reset_query();
                    $cat = get_field("choix_de_la_categorie3");

                    $the_query = new WP_Query("p=" . $cat->ID);

                    $the_query->the_post();
                    $title = get_the_title();
                    $desc = get_field("description_courte");
                    $img = get_field("banniere_haute");

                    ?>

                    <a href="<?php the_permalink(); ?>">
                        <div class="header">
                            <h2><?php echo $title; ?></h2>

                            <p><?php echo $desc; ?></p>
                        </div>
                        <?php if(!is_mobile()){ ?>
                            <div class="image" style="background-image: url(<?php echo $img["url"]; ?>)"></div>
                        <?php };?>
                    </a>
                </li>

            </ul>
        </div>
        <div class="postList postList_By2">
            <ul>
                <li>
                    <?php
                    wp_reset_query();
                    $cat = get_field("choix_de_la_categorie4");

                    $the_query = new WP_Query("p=" . $cat->ID);
                    if ($the_query->have_posts()) {
                        while ($the_query->have_posts()) {
                            $the_query->the_post();
                            $title = get_the_title();
                            $desc = get_field("description_courte");
                            $img = get_field("banniere_haute");

                            ?>

                            <a href="<?php the_permalink(); ?>">
                                <div class="header">
                                    <h2><?php echo $title; ?></h2>

                                    <p><?php echo $desc; ?></p>
                                </div>
                                <?php if(!is_mobile()){ ?>
                                    <div class="image" style="background-image: url(<?php echo $img["url"]; ?>)"></div>
                                <?php };?>
                            </a>

                        <?php
                        }
                    }
                    ?>
                </li>
                <li class="news">
                    <?php
                    wp_reset_query();

                    $the_query = new WP_Query("cat=19&limit=4");
                    if ($the_query->have_posts()) {
                        while ($the_query->have_posts()) {
                            $the_query->the_post();
                            $title = get_the_title();
                            $desc = get_field("description_courte");
                            $img = get_field("banniere_haute");

                            ?>
                            <div class="news_header">
                                <a href="<?php the_permalink();?>">
                                    <h2><?php the_title(); ?></h2>
                                    <p><?php echo $desc; ?></p>
                                </a>
                            </div>


                        <?php
                        }
                    }
                    ?>


                </li>
            </ul>
        </div>
    </div>


<?php
get_footer();
?>