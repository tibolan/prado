<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>
</main>
<footer>
    <ul class="legal">
        <li>@2013 LE PRADO. Tous droits réservés</li>
        <li><a href="#">Mentions légales</a></li>
        <!--li><a href="#">Plan du site</a></li-->
    </ul>
    <!--ul class="social">
        <li class="pinterest"><a href="">Pinterest</a></li>
        <li class="twitter"><a href="">Twitter</a></li>
        <li class="fb"><a href="">Facebook</a></li>
        <li class="gplus"><a href="">Google+</a></li>
        <li class="linkedin"><a href="">LinkedIn</a></li>
    </ul-->
</footer>

<div id="fog"><img src="/px.gif"/></div>


<script src="<?php bloginfo('template_directory'); ?>/webq/js/vendor/jquery-1.10.2.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/webq/js/vendor/jquery.flexslider.js"></script>


<script src="<?php bloginfo('template_directory'); ?>/webq/js/webq-browser.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/webq/js/main.js"></script>

<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-17068000-2', 'prado-shopping.fr');
    ga('send', 'pageview');

</script>
</body>
</html>