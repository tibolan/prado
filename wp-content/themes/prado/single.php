<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Prado
 * @since 1.0
 */

get_header(); ?>

<?php while (have_posts()) : the_post(); ?>

    <?php
    $banner = get_field("banniere_haute");

    if($banner && !is_mobile()){
        $size = wp_get_attachment_image_src($banner["id"], "full");
        $orientation = $size[1] > $size[2] ? 'landscape' : 'portrait';
        ?>

        <div class="banner <?php echo $orientation; ?>">
            <?php createImage($banner, $size, $orientation); ?>
        </div>
        <?php
    }


    $carrousel = get_field("carrousel");

    $carrousel_position = get_field("position_du_carrousel");
    $carrousel_built = false;
    $i = 1;


    foreach (get_field("bloc_de_contenu") as $bloc) {

        if ($i == $carrousel_position) {
            createCarrousel($carrousel, "full");
            $carrousel_built = true;
        }

        $imgs = $bloc['images'];

        /*

        echo "<pre>";
        var_dump($imgs);
        echo "</pre>";

        */

        $l = count($imgs);


        ?>
        <div class="block <?php echo ($bloc['position_des_images'] == 'Droite') ? 'onRight' : 'onLeft'; ?>">
            <div class="blockInner">

                <?php if ($i == 1) { ?> <h1><?php the_title(); ?></h1><?php }; ?>

                <div class="medias by<?php echo $l; ?> <?php echo $bloc['disposition_des_images'];?>">
                    <?php
                    $j = 0;

                    if ($l < 3) {
                        foreach ($imgs as $img) {

                            $orient = ($img['image']['width'] > $img['image']['height']) ? "landscape" : "portrait";


                            createPostAdvancedImage($img['image'], $img['title'], $img['description'], true, $l, $j, $orient);
                            echo "<span role='presentation' class='br br" . ($j + 1) . "'></span>";
                            $j++;
                        }
                    } else if($l < 4){
                        /**
                         * type3a : par 3, horizontal, grande image en haut
                        type3b : par 3, horizontal, grande image en bas
                        type3c : par 3, vertical, grande image à droite
                        type3d : par 3, vertical, grande image à gauche
                         *
                         */


                        ?>
                        <table class="<?php echo $bloc['disposition_des_images'];?>">
                            <tbody>
                            <?php
                                $type = $bloc['disposition_des_images'];

                            foreach ($imgs as $img) {

                                if($type == "type3a" || $type == "type3b"){
                                    if($j == 0){
                                        $colspan = "colspan='2'";
                                        $rowspan = "";
                                    }
                                    else {
                                        $colspan = "";
                                        $rowspan = "";
                                    }
                                }
                                else if($type == "type3c" || $type == "type3d"){
                                    if($j == 0){
                                        $colspan = "";
                                        $rowspan = "rowspan='2'";
                                    }
                                    else {
                                        $colspan = "";
                                        $rowspan = "";
                                    }
                                }







                                if($j == 0){
                                    echo "<tr>";
                                }

                                echo "<td $colspan $rowspan $j>";
                                createPostAdvancedImage($img['image'], $img['title'], $img['description'], true, $l, $j);
                                echo "</td>";

                                if($j == 0 && $type != "type3c" && $type != "type3d"){
                                    echo "</tr><tr>";
                                }
                                else if ($j == 1 && ($type == "type3c" || $type == "type3d")){
                                    echo "</tr><tr>";
                                }



                                if($j == $l){
                                    echo "</tr>";
                                }

                                $j++;
                            }

                            ?></tbody>
                        </table>
                    <?php
                    }
                    else {



                        ?>
                        <table class="<?php echo $bloc['disposition_des_images'];?>">
                            <tbody>
                            <?php
                            $type = $bloc['disposition_des_images'];

                            foreach ($imgs as $img) {



                                if($j == 0){
                                    echo "<tr>";
                                }

                                echo "<td $colspan $rowspan $j>";
                                createPostAdvancedImage($img['image'], $img['title'], $img['description'], true, $l, $j);
                                echo "</td>";

                                if($j == 1){
                                    echo "</tr><tr>";
                                }
                                else if($j == $l){
                                    echo "</tr>";
                                }

                                $j++;
                            }

                            ?></tbody>
                        </table><?php
                    }
                    ?>
                </div>
                <div class="rte">
                    <?php echo $bloc["contenu_texte"]; ?>
                </div>
            </div>
        </div>
        <?php

        $i++;
    }

    if (!$carrousel_built && $carrousel) {
        createCarrousel($carrousel, "full");
        $carrousel_built = true;
    }

endwhile; // end of the loop.

get_footer();

?>