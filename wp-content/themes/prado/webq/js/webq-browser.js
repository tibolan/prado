(typeof webq == "undefined") && (webq = {});

webq.browser = {};
(function() {
    var c = {}, a = navigator.userAgent, m = document.documentElement, g, d;
    m.removeAttribute("id");
    m.className += " beforeLoading hasJS";
    var k = [{exp: /MSIE ([.\d]+)/i,name: "IE",cssPrefix: "-ms-",jsPrefix: "ms"}, {exp: /Firefox\/([.\d]+)/i,name: "FF",cssPrefix: "-moz-",jsPrefix: "Moz"}, {exp: /Chrome\/([.\d]+)/i,name: "Chrome",cssPrefix: "-webkit-",jsPrefix: "Webkit"}, {exp: /Opera.*Version\/([.\d]+)/i,name: "Opera",cssPrefix: "-o-",jsPrefix: "O"}, {exp: /NokiaBrowser\/([.\d]+)/i,name: "NokiaBrowser",cssPrefix: "-webkit-",jsPrefix: "Webkit"}, {exp: /Version\/([.\d]+).*Safari/i,name: "Safari",cssPrefix: "-webkit-",jsPrefix: "Webkit"}];
    for (var h = 0, f = k.length; h < f; h++) {
        var d = k[h];
        if (g = a.match(d.exp)) {
            c.browser = d.name;
            c.browserVersionFull = g[1];
            c.browserVersion = c.browserVersionFull.split(".")[0];
            c.cssPrefix = d.cssPrefix;
            c.jsPrefix = d.jsPrefix
        }
    }
    var j = [{exp: /Windows NT (\d\.\d)/i,name: "Windows"}, {exp: /mac OS X (10[._]\d)/i,name: "macOSX"}, {exp: /Linux/i,name: "Linux"}, {exp: /Android ([._\d]+)/i,name: "Android"}, {exp: /BlackBerry/i,name: "BlackBerry"}, {exp: /([\._\d]+) like Mac OS X/i,name: "iOS"}, {exp: /Opera Mini/i,name: "OperaMini"}, {exp: /IEMobile/i,name: "WindowsMobile"}, {exp: /MeeGo/i,name: "MeeGo"}];
    for (var h = 0, f = j.length; h < f; h++) {
        var d = j[h];
        if (g = a.match(d.exp)) {
            c.platform = d.name;
            c.platformVersionFull = g[1] ? g[1].replace(/_/g, ".") : "";
            c.platformVersion = c.platformVersionFull.split(".")[0]
        }
    }
    switch (c.platform) {
        case "Windows":
            c.platformName = "Windows " + (c.platformVersionFull == "6.2" ? "8" : c.platformVersionFull == "6.1" ? "7" : c.platformVersionFull == "6.0" ? "Vista" : c.platformVersionFull == "5.2" ? "XP x64" : c.platformVersionFull == "5.1" ? "7" : "XP");
            break;
        case "linux":
            // TODO better sniffing
            c.platformName = "Linux";
            break;
        case "macOSX":
            c.platformName = "" + (c.platformVersionFull == "10.2" ? "Jaguar" : c.platformVersionFull == "10.3" ? "Panther" : c.platformVersionFull == "10.4" ? "Tiger" : c.platformVersionFull == "10.5" ? "Leopard" : c.platformVersionFull == "10.6" ? "SnowLeopard" : c.platformVersionFull == "10.7" ? "Lion" :  c.platformVersionFull == "10.8" ? "mountainLion" : "");
            break;
        case "iOS":
            c.platformName = "iOS " + c.platformVersionFull.split(".")[0];
            c.device = a.match(/(iPhone|iPad|iPod)/i)[1];
            break
    }
    if (c.device) {
        c["is" + c.device] = true
    }
    if (c.platform) {
        c["is" + c.platform] = true
    }
    if (c.platformName) {
        c["is" + c.platformName.replace(/\s/, "")] = true
    }
    if (c.browser) {
        c["is" + c.browser] = true
    }
    if (c.browser && c.browserVersion) {
        c["is" + c.browser + c.browserVersion] = true
    }
    c.isMobile = c.isAndroid || c.isBlackBerry || c.isiOS || c.isOperaMini || c.isWindowsMobile || false;
    c.isTouch = ("ontouchstart" in window) || (window.DocumentTouch && document instanceof DocumentTouch) || ((c.isIE10) ? a.match(/Touch/i) : !!("onmsgesturechange" in window));


    c.positionFixed = false;
    d = document.createElement("div");
    d.style.position = "fixed";



    for (var b in c) {
        if (/^is/.test(b)) {
            if (c[b]) {
                m.className += " " + b
            } else {
                m.className += " isNot" + b.split("is")[1]
            }
        }
    }

    var getViewport = function() {
        var l = window, i = "inner";
        if (!("innerWidth" in window)) {
            i = "client";
            l = document.documentElement || document.body
        }
        return {width: l[i + "Width"],height: l[i + "Height"],colorDepth: window.screen.colorDepth,availWidth: window.screen.availWidth,availHeight: window.screen.availHeight}
    };
    function sync() {
        webq.viewPort = getViewport()
    }
    jQuery(window).on("resize orientationchange",sync);
    sync();
    webq.browser = $.extend({}, c)
})();