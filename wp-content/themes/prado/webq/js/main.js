$(function () {
    // NAV


    $("#nav > ul > li").each(function (index, li) {


        var to = null,
            li = $(li),
            as = li.find("a"),
            navFocusIn = function (e) {
                if (navFocusOutTimeout) clearTimeout(navFocusOutTimeout);
                $("#nav ul ul li").removeClass("hover");
                $(this).parents("li").addClass("hover");
            },
            navFocusOut = function (e) {
                var $this = this;
                navFocusOutTimeout = setTimeout(function () {
                    $($this).parents("li").removeClass("hover");
                }, 300);
            };
        var navFocusOutTimeout = null;

        $(as).on("focusin mouseenter", navFocusIn)
            .on("focusout mouseleave", navFocusOut);
    });
    if (document.location.pathname.length > 1) $("nav a[href$='" + document.location.pathname + "']").addClass("current").parents("li").addClass("current");


    $('.carrousel').each(function (index, carrousel) {
        var $carrousel = $(carrousel), flexslider;
        $carrousel.flexslider({
            animation: "slide",
            smoothHeight: false,
            itemWidth: getCarrouselItemWidth(),
            slideshowSpeed: 4000,
            animationLoop: true,
            pauseOnHover: true,
            controlNav: true,
            itemMargin: getCarrouselItemMargin()

        });

        return;

        function getCarrouselItemWidth() {
            return $carrousel.width();
        }

        function getCarrouselItemMargin() {

            return parseInt(($carrousel.find("li:first").css("margin-right"))) * 2
        }

        function verticalAlignCarrouselItem() {
            var h = $carrousel.height();
            //debugger;
            $carrousel.find(".flex-viewport li").each(function (i, item) {
                item.style.marginTop = (h - item.offsetHeight) / 2 + "px";
            })
        }

        $(window).resize(function () {
            var flex;
            if (flex = $carrousel.data("flexslider")) {
                flex.vars.itemWidth = getCarrouselItemWidth();
                flex.vars.itemMargin = getCarrouselItemMargin();
                verticalAlignCarrouselItem();
            }


        });
        $(document).on("layoutChanged", function (e) {
            $carrousel.data("flexslider").vars.itemWidth = getCarrouselItemWidth();
            $carrousel.data("flexslider").vars.w = getCarrouselItemWidth();
            verticalAlignCarrouselItem();
        });


        setTimeout(verticalAlignCarrouselItem, 1000);
    })


    $(".full figcaption").append("<span class='tattoo'></span>");


    /* template hacking, no time to do better now. */
    $(".by3 .type3b").each(function (index, table) {
        table = $(table);
        table.find("tr:last").prependTo(table);
    });
    $(".by3 .type3c").each(function (index, table) {
        table = $(table);
        table.find("td").eq(1).prependTo(table.find("tr:first"));
    });

    $(".medias figure").on("click", function () {
        var $caption = $(this);
        var datas = JSON.parse($caption.find("[data-sizes]").attr("data-sizes"));


        var clone = $("<img src='/px.gif'/>");
        var w = $(window).width();
        var size = (Modernizr.touch) ? "medium" : "large";
        var src = datas[size];

        var d = src.match(/(\d+)x(\d+)\./);
        //alert(d[1] + "/" + d[2]);
        if (!d) {
            src = datas["natural"];
            d = ["", datas["natural-width"], datas["natural-height"]]
        }
        var W = +d[1];
        var H = +d[2];

        var orientation = (W > H) ? "landscape" : "portrait";
        clone.attr("src", src).addClass(orientation).on("load", function () {
            clone.css({
                opacity:0
            });
            $("#fog").empty().append(clone);
            $(this).addClass("loaded");
            var tmp = clone[0].offsetHeight;
            clone.css({
                opacity:1
            });


        })


        $("#fog").empty().append("<img src='/px.gif'/>");
        $(document.documentElement).addClass("fogOpen");
    })

    $("#fog").on("click", function () {
        $(document.documentElement).removeClass("fogOpen");
    });

    $("#fog img").on("click", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
    });

    $("#espacePro .opener").on("click", function (e) {
        e.preventDefault();
        $("#login").addClass("open");
    });
    $("#loginClose").on("click", function (e) {
        e.preventDefault();
        $("#login").removeClass("open");
    });

    $(".flex-control-nav a").attr("tabindex", 0);


    if(document.location.hash.match(/wpcf7/)) $("#login").addClass("open");

    $(".wpcf7-form").removeAttr("novalidate");

    if(webq.browser.isMobile){
    $("#showNav").click(function (e){
        console.log("click", this)
        e.preventDefault();
        $(this).parent().find("> ul").toggleClass("open");
        $(this).closest("header").toggleClass("open");
    })
    }
});


$(window).load(function (){
    $(document).trigger("layoutChanged");
    setTimeout(function (){
        $(window).trigger("resize");
    }, 1000);
})
