<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', '/Users/tibolan/Sites/prado/wp-content/plugins/wp-super-cache/' ); //Added by WP-Cache Manager
define('DB_NAME', 'prado-wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '#tR9C{?e`6)pxpI{!7y;Xab).|j(%O7+Bu)X/~$+qjC@&C&CT_zQ-;p ,- JHT8=');
define('SECURE_AUTH_KEY',  '2Q61()CT.0f4NLawFV-#lU1)vvX*FVvC@~|${feI|U~m<OhTi]n!X`lPVWLSY00^');
define('LOGGED_IN_KEY',    '^-KfG/l@ri3V@/A@WaZ%:>ElZu&W;bHyU@f}lq8){5pF{=g^l:fZA]zSm`fNe_dw');
define('NONCE_KEY',        '$S.Mu.Vn6hFrMh0~pz-pk,[f@~M*I>k9vd#d/}+-HLO]I[P?wO8 ^dIE4y3C&Z!!');
define('AUTH_SALT',        '.U>sC{+m~<!XeJ%)7Dw/J}CmT9t{SN]ii=|m*xU>[YDgVv@R9]|G|:j&^o$FM9H-');
define('SECURE_AUTH_SALT', 'w5@6U[Lk>G%w,KVykbDgffLs[s+l}=]NJ+}}F->#Ty0qJUyFO+31y4QnF<4H6Vic');
define('LOGGED_IN_SALT',   'o-ZBhBv26-BOy*Hfe)|YbCHS;a5{Vj)Ja |y=FXirC+r_h6}ywa9z1woO;!O^YVn');
define('NONCE_SALT',       'D XY%)PmA_u[+<0_ba@*7;OoQ$<)_vmkFX@+[$.?V)C+#QUo:?4)T8G|lZ;og|/_');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
